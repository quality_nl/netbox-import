import csv
import pynetbox
import ipaddress


def read_csv(import_file):
    csv_data = list()

    # Open the CSV file
    with open(import_file, 'r') as csv_file:
        # Set CSV properties
        csv_reader = csv.reader(csv_file, delimiter=',')

        # Skip the header
        next(csv_reader)

        # Add the csv_data to a new list
        for row in csv_reader:
            csv_data.append(row)

    # Return the new CSV data
    return csv_data


def get_or_create_vlan(nb, vlan_id, vlan_description):
    # Check if the VLAN exists
    nb_vlan = nb.ipam.vlans.get(vid=vlan_id)

    # If not create the VLAN
    if not nb_vlan:
        nb_vlan = nb.ipam.vlans.create(
            vid=vlan_id,
            name=vlan_description
        )

        # Print the created VLAN
        fmt = '{:<20}{:<20}'
        header = ('VLAN', 'Description')
        print('The following VLAN is created:')
        print(fmt.format(*header))
        print(fmt.format(
            str(nb_vlan.vid),
            nb_vlan.name
        ))
        print('')

    # Return the netbox object id
    return nb_vlan


def get_or_create_prefix(nb, nb_vlan, prefix, prefix_description):
    # Check if the prefix exists
    nb_prefix = nb.ipam.prefixes.get(prefix=prefix)

    # If not create the prefix
    if not nb_prefix:
        nb_prefix = nb.ipam.prefixes.create(
            prefix=prefix,
            description=prefix_description,
            vlan=nb_vlan.id
        )

        # Print the created Prefix
        fmt = '{:<30}{:<30}{:<30}'
        header = ('VLAN', 'Prefix', 'Description')
        print('The following Prefix is created:')
        print(fmt.format(*header))
        print(fmt.format(
            str(nb_prefix.vlan.display),
            nb_prefix.prefix,
            nb_prefix.description
        ))
        print('')

    # Return the netbox object id
    return nb_prefix


def get_or_create_address(nb, prefix, ip_address, ip_description):
    ip_address_cidr = '{}/{}'.format(ip_address,
                                     ipaddress.ip_network(prefix).prefixlen)

    nb_ip_address = nb.ipam.ip_addresses.get(address=ip_address_cidr)

    if not nb_ip_address:
        nb_ip_address = nb.ipam.ip_addresses.create(
            address=ip_address_cidr,
            description=ip_description
        )

        # Print the created IP Address
        fmt = '{:<30}{:<30}'
        header = ('IP Address', 'Description')
        print('The following IP address is created:')
        print(fmt.format(*header))
        print(fmt.format(
            str(nb_ip_address.address),
            nb_ip_address.description
        ))
        print('')

    return nb_ip_address


if __name__ == '__main__':
    # Variables
    netbox_url = 'https://netbox.url'
    netbox_token = 'abcd1234abcd1234abcd1234'
    import_file = 'import.csv'

    # Read the CSV data
    csv_data = read_csv(import_file)

    # Open a connection to Netbox
    nb = pynetbox.api(netbox_url, token=netbox_token)

    for entry in csv_data:
        # Parse the list to variables
        vlan_id = entry[0]
        prefix = entry[1]
        prefix_description = entry[2]
        ip_address = entry[3]
        ip_description = entry[4]

        # If not exists, create the vlan
        nb_vlan = get_or_create_vlan(
            nb=nb,
            vlan_id=vlan_id,
            vlan_description=prefix_description
        )

        # If not exists, create the prefix
        nb_prefix = get_or_create_prefix(
            nb=nb,
            nb_vlan=nb_vlan,
            prefix=prefix,
            prefix_description=prefix_description
        )

        # If not exists, create the address
        nb_address = get_or_create_address(
            nb=nb,
            prefix=prefix,
            ip_address=ip_address,
            ip_description=ip_description
        )
